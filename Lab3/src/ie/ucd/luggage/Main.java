package ie.ucd.luggage;

public class Main {

	public static void main(String[] args) {
		Pen newPen = new Pen();
		Pen newDesignerPen = new DesignerPen("Branding");
		Laptop newLaptop = new Laptop();
		SafeLuggage newSafeLuggage = new SafeLuggage("Open");
		Bomb newBomb = new Bomb();
		
		newSafeLuggage.EnterPassword();
		
		newSafeLuggage.add(newLaptop);
		newSafeLuggage.add(newPen);
		newSafeLuggage.add(newBomb);
		newSafeLuggage.add(newDesignerPen);
		
		System.out.println("Is the luggage dangerous? " + newSafeLuggage.isDangerous());
		
		newSafeLuggage.removeItem(0);
		newSafeLuggage.removeItem(1);
		newSafeLuggage.removeItem(0);
		
		System.out.println(newSafeLuggage.isDangerous());
		
	}
}
