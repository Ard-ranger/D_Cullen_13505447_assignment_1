package ie.ucd.luggage;

public class Pen implements Item {
	
	private double weight=5.0;
	private String type = "Pen";
	private boolean danger  = false; 
	
	public Pen(){
	
	}	
	
	public String getType(){
		return type;
	}
	
	public double getWeight() {
		return weight;
	}
	
	public boolean isDangerous(){
		return false; 
	}
	
	
}
