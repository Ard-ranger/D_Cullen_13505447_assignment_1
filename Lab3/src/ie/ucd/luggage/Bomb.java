package ie.ucd.luggage;

public class Bomb implements Item{
	
	public Bomb(){
		
	}	

	public String getType(){
		return "Bomb";
	}
	
	public double getWeight() {
		return 5.00;
	}
	
	public boolean isDangerous(){
		return true; 
	}
}
