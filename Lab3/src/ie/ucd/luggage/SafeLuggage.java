package ie.ucd.luggage;

import java.util.Scanner;

public class SafeLuggage extends Suitcase{
	
	
	private String password = "Open";
	private String password_attempt = "Default";
	
	public SafeLuggage(String password){
		this.password = password;
	}
	
	public void EnterPassword(){
		System.out.print("Please enter your password: ");  
		Scanner scan = new Scanner(System.in);
		this.password_attempt = scan.nextLine();
	}
	
	
	public void add(Item item){
		if (password_attempt.equals(password)){
			super.add(item);
			System.out.println("Added item");
		}else{
			System.out.println("Password error");
		}
	}
	
	public void removeItem(int index){
		if ( password_attempt.equals(password)){
			super.removeItem(index);
			System.out.println("Removed item");
		}else if (password_attempt == "Default"){
			System.out.println("No password entered error");
		}
	}
	
	
	public boolean isDangerous(){
		for(Item i: items){
			if (i.isDangerous()){
				return true;
			}
		}
		return false;
	}
}
