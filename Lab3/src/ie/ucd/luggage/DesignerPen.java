package ie.ucd.luggage;

public class DesignerPen extends Pen {

	private String brand;
	
	public DesignerPen(String brand){
		this.brand = brand;
	}
	
	public String getType(){
		return "DesignerPen";
	}
	
}
