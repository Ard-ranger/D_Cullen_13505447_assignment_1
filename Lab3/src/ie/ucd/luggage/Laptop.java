package ie.ucd.luggage;

public class Laptop implements Item{
	
	
	public Laptop(){
		
	}	
	
	public String getType(){
		return "Laptop";
	}
	
	public double getWeight() {
		return 5.00;
	}
	
	public boolean isDangerous(){
		return false; 
	}
}