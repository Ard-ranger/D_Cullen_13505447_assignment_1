package ie.ucd.luggage;

public class Suitcase extends Luggage {
	
	public double getBagWeight() {
		return 5.00;
	}
	
	public String getType(){
		return "Suitcase";
	}
	
	public double getMaxWeight() {
		return 10.00;
	}
	
	public boolean isDangerous(){
		return false; 
	}
}
